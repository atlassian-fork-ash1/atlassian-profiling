package com.atlassian.util.profiling.dropwizard;

import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;
import com.codahale.metrics.ExponentiallyDecayingReservoir;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

import javax.annotation.Nonnull;

import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * Simple {@link MetricStrategy} that delegates to matching {@link Timer timers} from a DropWizard
 * {@link MetricRegistry}
 */
public class DropwizardMetricStrategy implements MetricStrategy {

    private static final int DEFAULT_RESERVOIR_SIZE = 2048;
    private static final int DEFAULT_MAX_NUMBER_OF_METRICS = 128;

    private final int maxNumberOfMetrics;
    private final MetricRegistry registry;
    private final int reservoirSize;

    public DropwizardMetricStrategy(@Nonnull MetricRegistry registry) {
        this.registry = requireNonNull(registry, "registry");

        maxNumberOfMetrics = DEFAULT_MAX_NUMBER_OF_METRICS;
        reservoirSize = DEFAULT_RESERVOIR_SIZE;
    }

    private DropwizardMetricStrategy(Builder builder) {
        maxNumberOfMetrics = builder.maxNumberOfMetrics;
        registry = builder.registry;
        reservoirSize = builder.reservoirSize;
    }

    @Override
    public void setConfiguration(@Nonnull MetricsConfiguration configuration) {
        // no-op
    }

    @Nonnull
    @Override
    public Ticker startTimer(@Nonnull String metricName) {
        Timer timer = registry.getTimers().get(metricName);
        if (timer == null && registry.getMetrics().size() < maxNumberOfMetrics) {
            timer = registry.timer(metricName, () -> new Timer(new ExponentiallyDecayingReservoir(reservoirSize, 0.015)));
        }
        if (timer == null) {
            return Ticker.NO_OP;
        }
        Timer.Context context = timer.time();
        return context::close;
    }

    @Override
    public void updateHistogram(@Nonnull String metricName, long value) {
        Histogram histogram = registry.getHistograms().get(metricName);
        if (histogram == null && registry.getMetrics().size() < maxNumberOfMetrics) {
            histogram = registry.histogram(metricName,
                    () -> new Histogram(new ExponentiallyDecayingReservoir(reservoirSize, 0.015)));
        }
        if (histogram != null) {
            histogram.update(value);
        }
    }

    @Override
    public void updateTimer(@Nonnull String metricName, long time, @Nonnull TimeUnit timeUnit) {
        registry.timer(metricName).update(time, timeUnit);
    }

    /**
     * @since 3.2
     */
    public static class Builder {

        private final MetricRegistry registry;

        private int maxNumberOfMetrics;
        private int reservoirSize;

        public Builder(@Nonnull MetricRegistry registry) {
            this.registry = requireNonNull(registry, "registry");

            maxNumberOfMetrics = DEFAULT_MAX_NUMBER_OF_METRICS;
            reservoirSize = DEFAULT_RESERVOIR_SIZE;
        }

        @Nonnull
        public DropwizardMetricStrategy build() {
            return new DropwizardMetricStrategy(this);
        }

        /**
         * Sets the maximum number of metrics allowed in the underlying Codahale {@link MetricRegistry registry}. The
         * strategy will not add new metrics to the registry when the registry contains the maximum number of allowed
         * metrics, or more. This can be used to limit the amount of memory used for metrics. Using the default
         * settings, expect about 250kb of memory usage per timer.
         * The default value is 128 (which translates to about 32MB of memory).
         *
         * @param value the maximum number of metrics that can be created by the strategy
         * @return the builder
         */
        @Nonnull
        public Builder maxNumberOfMetrics(int value) {
            maxNumberOfMetrics = value;
            return this;
        }

        /**
         * Sets the reservoir size used for calculating statistics for timers and histograms. Default value is 2048.
         *
         * @param value the reservoir size to use
         * @return the builder
         */
        @Nonnull
        public Builder reservoirSize(int value) {
            reservoirSize = value;
            return this;
        }
    }
}
