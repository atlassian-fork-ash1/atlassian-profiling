package com.atlassian.util.profiling.micrometer;

import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Simple {@link MetricStrategy} that delegates to matching {@link Timer timers} from a Micrometer {@link MeterRegistry}.
 */
@ParametersAreNonnullByDefault
public class MicrometerStrategy implements MetricStrategy {

    private final MeterRegistry registry;

    public MicrometerStrategy(MeterRegistry registry) {
        this.registry = requireNonNull(registry, "registry");
    }

    @Nonnull
    @Override
    public Ticker startTimer(String metricName) {
        return startTimer(registry.timer(metricName));
    }

    @Nonnull
    @Override
    public Ticker startTimer(MetricKey metricKey) {
        return startTimer(registry.timer(metricKey.getMetricName(), getTags(metricKey)));
    }

    private Ticker startTimer(Timer timer) {
        final Timer.Sample sample = Timer.start(registry);
        return () -> sample.stop(timer);
    }

    @Override
    public void updateHistogram(String metricName, long value) {
        registry.summary(metricName).record(value);
    }

    @Override
    public void updateHistogram(MetricKey metricKey, long value) {
        registry.summary(metricKey.getMetricName(), getTags(metricKey)).record(value);
    }

    @Override
    public void updateTimer(MetricKey metricKey, Duration time) {
        registry.timer(metricKey.getMetricName(), getTags(metricKey)).record(time);
    }

    @Override
    public void updateTimer(String metricName, long time, TimeUnit timeUnit) {
        registry.timer(metricName).record(time, timeUnit);
    }

    /**
     * Build a list of Micrometer {@link Tag}s from the given {@link MetricKey}.
     */
    private static List<Tag> getTags(MetricKey metricKey) {
        return metricKey.getTags()
                        .stream()
                        .map(tag -> Tag.of(tag.getKey(), tag.getValue()))
                        .collect(toList());
    }
}