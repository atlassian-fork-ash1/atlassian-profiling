package com.atlassian.util.profiling;

import com.atlassian.util.profiling.strategy.MetricStrategy;
import com.atlassian.util.profiling.strategy.ProfilerStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;

/**
 * Factory and utility methods for create {@link Timer} instances
 *
 * <pre>
 *     try (Ticker ignored = Timers.start("my-timer")) {
 *         // monitored code block here
 *     }
 * </pre>
 *
 * @since 3.0
 */
public class Timers {
    private static final ProfilerConfiguration CONFIGURATION = new ProfilerConfiguration();
    private static final Logger log = LoggerFactory.getLogger(Timers.class);

    private Timers() {
        throw new UnsupportedOperationException("Timers is an utility class and should not be instantiated");
    }

    @Nonnull
    public static Timer concat(@Nonnull Timer... timers) {
        return new CompositeTimer(timers);
    }

    @Nonnull
    public static ProfilerConfiguration getConfiguration() {
        return CONFIGURATION;
    }

    /**
     * Call this method to signal that the request has completed to trigger all configured
     * {@link ProfilerStrategy profiler strategies} and {@link MetricStrategy metric strategies} to clean up any
     * request or thread scoped resources associated with profiling state, including {@link Ticker#close() closing}
     * any {@link Ticker tickers} that have not yet been closed.
     */
    public static void onRequestEnd() {
        for (ProfilerStrategy strategy : StrategiesRegistry.getProfilerStrategies()) {
            try {
                strategy.onRequestEnd();
            } catch (Exception e) {
                log.warn("Error cleaning up profiler state for {}", strategy.getClass().getName(), e);
            }
        }

        for (MetricStrategy strategy : StrategiesRegistry.getMetricStrategies()) {
            try {
                strategy.onRequestEnd();
            } catch (Exception e) {
                log.warn("Error cleaning up metrics state for {}", strategy.getClass().getName(), e);
            }
        }
    }

    @Nonnull
    public static Ticker start(@Nonnull String name) {
        if (CONFIGURATION.isEnabled()) {
            return timer(name).start();
        }
        return Ticker.NO_OP;
    }

    @Nonnull
    public static Ticker startWithMetric(@Nonnull String timerName) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName).start();
        }
        return Metrics.startTimer(timerName);
    }

    @Nonnull
    public static Ticker startWithMetric(@Nonnull String timerName, @Nonnull String metricName) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName, metricName).start();
        }
        return Metrics.startTimer(metricName);
    }

    /**
     * @since 3.4.2
     */
    @Nonnull
    public static Ticker startWithMetric(@Nonnull String timerName, @Nonnull MetricTag... metricTags) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName, metricTags).start();
        }
        return Metrics.startTimer(timerName, metricTags);
    }

    /**
     * @since 3.4.1
     */
    @Nonnull
    public static Ticker startWithMetric(@Nonnull String timerName, @Nonnull String metricName, @Nonnull MetricTag... metricTags) {
        if (CONFIGURATION.isEnabled()) {
            return timerWithMetric(timerName, metricName, metricTags).start();
        }
        return Metrics.startTimer(metricName, metricTags);
    }

    @Nonnull
    public static Timer timer(@Nonnull String name) {
        return new DefaultTimer(requireNonNull(name, "name"));
    }

    @Nonnull
    public static Timer timerWithMetric(@Nonnull String traceName, @Nonnull String metricName) {
        return timerWithMetric(traceName, metricName, emptySet());
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Timer timerWithMetric(@Nonnull String traceName, @Nonnull String metricName, @Nonnull MetricTag... tags) {
        return timerWithMetric(traceName, metricName, asList(tags));
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Timer timerWithMetric(@Nonnull String traceName, @Nonnull String metricName, @Nonnull Collection<MetricTag> tags) {
        Timer timer = Timers.timer(traceName);
        MetricTimer metricTimer = Metrics.timer(metricName, tags);

        return new Timer() {
            @Nonnull
            @Override
            public Ticker start(@Nullable Object... callParameters) {
                Ticker traceTicker = timer.start(callParameters);
                Ticker metricTicker = metricTimer.start();
                return Tickers.of(traceTicker, metricTicker);
            }
        };
    }

    @Nonnull
    public static Timer timerWithMetric(@Nonnull String name) {
        return timerWithMetric(name, name);
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Timer timerWithMetric(@Nonnull String name, @Nonnull Collection<MetricTag> tags) {
        return timerWithMetric(name, name, tags);
    }

    /**
     * @since 3.4
     */
    @Nonnull
    public static Timer timerWithMetric(@Nonnull String name, @Nonnull MetricTag... tags) {
        return timerWithMetric(name, name, asList(tags));
    }

    private static class CompositeTimer implements Timer {

        private final Timer[] timers;

        private CompositeTimer(@Nonnull Timer[] timers) {
            this.timers = requireNonNull(timers, "timers");
        }

        @Nonnull
        @Override
        public Ticker start(@Nullable Object... callParameters) {
            CompositeTicker ticker = null;
            for (Timer timer : timers) {
                if (timer != null) {
                    ticker = Tickers.addTicker(timer.start(callParameters), ticker);
                }
            }
            return ticker == null ? Ticker.NO_OP : ticker;
        }
    }

    private static class DefaultTimer implements Timer {
        private static final Logger log = LoggerFactory.getLogger(DefaultTimer.class);

        private final String name;

        DefaultTimer(String name) {
            this.name = name;
        }

        @Nonnull
        @Override
        public Ticker start(@Nullable Object... callParameters) {
            if (!CONFIGURATION.isEnabled()) {
                return Ticker.NO_OP;
            }

            String frameName;
            if (callParameters == null || callParameters.length == 0) {
                frameName = name;
            } else {
                StringBuilder builder = new StringBuilder(name);

                boolean added = false;
                for (Object p : callParameters) {
                    String param = p == null ? null : String.valueOf(p);
                    if (!(param == null || param.isEmpty())) {
                        if (added) {
                            builder.append(", ");
                        } else {
                            builder.append("(");
                            added = true;
                        }
                        builder.append(param);
                    }
                }
                if (added) {
                    builder.append(")");
                }

                frameName = builder.toString();
            }

            CompositeTicker compositeTicker = null;
            for (ProfilerStrategy strategy : StrategiesRegistry.getProfilerStrategies()) {
                try {
                    compositeTicker = Tickers.addTicker(strategy.start(frameName), compositeTicker);
                } catch (Exception e) {
                    log.warn("Failed to start profiling frame for {}", frameName, e);
                }
            }
            return compositeTicker == null ? Ticker.NO_OP : compositeTicker;
        }
    }
}
