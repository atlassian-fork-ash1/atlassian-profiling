package com.atlassian.util.profiling;

/**
 * Factory and utility methods for composing {@link Ticker}.
 * <p>
 * <pre>
 *     try (Ticker ignored = Tickers.of(Timers.start("my-timer"), Metrics.start("my-timer"))) {
 *         // monitored code block here
 *     }
 * </pre>
 *
 * @since 3.0
 */
class Tickers {

    private Tickers() {
        throw new UnsupportedOperationException("Tickers is an utility class and should not be instantiated");
    }

    static Ticker of(Ticker ticker1, Ticker ticker2) {
        if (ticker1 == Ticker.NO_OP || ticker1 == null) {
            return ticker2;
        }
        if (ticker2 == Ticker.NO_OP || ticker2 == null) {
            return ticker1;
        }
        return new CompositeTicker(ticker1, ticker2);
    }

    public static Ticker of(Ticker ticker1, Ticker... tickers) {
        CompositeTicker result = addTicker(ticker1, null);
        for (Ticker ticker : tickers) {
            result = addTicker(ticker, result);
        }
        return result == null ? Ticker.NO_OP : result;
    }

    static CompositeTicker addTicker(Ticker ticker, CompositeTicker compositeTicker) {
        if (ticker == null || ticker == Ticker.NO_OP) {
            // nothing to add
            return compositeTicker;
        }
        CompositeTicker result = compositeTicker == null ? new CompositeTicker() : compositeTicker;
        result.add(ticker);
        return result;
    }
}
