package com.atlassian.util.profiling.strategy.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Bean to contain information about the pages profiled
 *
 * @author <a href="mailto:mike@atlassian.com">Mike Cannon-Brookes</a>
 * @author <a href="mailto:scott@atlassian.com">Scott Farquhar</a>
 * @deprecated since 3.0 for removal in 4.0
 */
@Deprecated
public class ProfilingTimerBean implements java.io.Serializable {

    List<ProfilingTimerBean> children = new ArrayList<>();
    int frameCount;
    boolean hasMem = false;
    ProfilingTimerBean parent = null;
    String resource;
    long startMem;
    long startTime;
    long totalMem;
    long totalTime;

    public ProfilingTimerBean(String resource) {
        this.resource = resource;
    }

    public void addChild(ProfilingTimerBean child) {
        children.add(child);
        child.addParent(this);
    }

    public ProfilingTimerBean getParent() {
        return parent;
    }

    /**
     * Get a formatted string representing all the methods that took longer than a specified time.
     *
     * @param minTime time to be formatted
     * @return formatted string
     */
    public String getPrintable(long minTime) {
        return getPrintable("", minTime);
    }

    public String getResource() {
        return resource;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setEndMem() {
        // Only capture total memory if we captured the startMem
        if (hasMem) {
            this.totalMem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) - startMem;
        }
    }

    public void setEndTime() {
        this.totalTime = System.currentTimeMillis() - startTime;
    }

    public void setStartMem() {
        this.startMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        this.hasMem = true;
    }

    public void setStartTime() {
        this.startTime = System.currentTimeMillis();
    }

    protected void addParent(ProfilingTimerBean parent) {
        this.parent = parent;
    }

    protected String getPrintable(String indent, long minTime) {
        //only print the value if we are larger or equal to the min time.
        if (totalTime >= minTime) {
            StringBuilder builder = new StringBuilder();
            builder.append(indent);
            builder.append("[").append(totalTime).append("ms] ");
            if (hasMem) {
                builder.append("[").append(totalMem / 1024).append("KB used] ");
                builder.append("[").append(Runtime.getRuntime().freeMemory() / 1024).append("KB Free] ");
            }

            builder.append("- ").append(resource);
            builder.append("\n");

            for (ProfilingTimerBean aChildren : children) {
                builder.append(aChildren.getPrintable(indent + "  ", minTime));
            }

            return builder.toString();
        } else {
            return "";
        }
    }

    int getFrameCount() {
        return frameCount;
    }

    void removeChild(ProfilingTimerBean child) {
        ListIterator<ProfilingTimerBean> childrenIt = children.listIterator(children.size());
        while (childrenIt.hasPrevious()) {
            ProfilingTimerBean time = childrenIt.previous();
            if (time == child) {
                childrenIt.remove();
                return;
            }
        }
    }

    void setFrameCount(int frameCount) {
        this.frameCount = frameCount;
    }
}

