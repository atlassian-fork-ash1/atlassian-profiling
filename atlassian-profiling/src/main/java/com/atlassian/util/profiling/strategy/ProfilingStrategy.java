package com.atlassian.util.profiling.strategy;

import com.atlassian.util.profiling.Timers;

/**
 * Basic interface for profiling strategies, which are used in {@link com.atlassian.util.profiling.UtilTimerStack}.
 *
 * @since 2.0
 * @deprecated in 3.0 for removal in 4.0. Use {@link ProfilerStrategy} and {@link Timers}
 *             instead.
 */
@Deprecated
public interface ProfilingStrategy {
    /**
     * Return <code>true</code> if this strategy is enabled, <code>false</code> otherwise.
     *
     * @return <code>true</code> if this strategy is enabled, <code>false</code> otherwise.
     */
    boolean isEnabled();

    /**
     * This method is called on a start of the code snippet profiling
     *
     * @param tag name
     */
    void start(String tag);

    /**
     * This method is called on a stop of the code snippet profiling
     *
     * @param tag name
     */
    void stop(String tag);
}
