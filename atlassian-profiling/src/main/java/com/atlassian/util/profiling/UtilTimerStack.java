package com.atlassian.util.profiling;

import com.atlassian.util.profiling.strategy.ProfilingStrategy;
import com.atlassian.util.profiling.strategy.impl.StackProfilingStrategy;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A timer stack.
 * <p>
 * Usage:
 * <pre>
 * String logMessage = "Log message";
 * UtilTimerStack.push(logMessage);
 * try {
 *     //do some code
 * } finally {
 *     UtilTimerStack.pop(logMessage); //this needs to be the same text as above
 * }
 * </pre>
 *
 * @deprecated in 3.0 to be removed in 4.0. Use {@link Timers instead}
 */
@Deprecated
public class UtilTimerStack {

    private static final ConcurrentLinkedQueue<ProfilingStrategy> strategies = new ConcurrentLinkedQueue<>();

    private static StackProfilingStrategy defaultProfilingStrategy =
            new StackProfilingStrategy(StrategiesRegistry.getDefaultProfilerStrategy());

    public static void add(ProfilingStrategy strategy) {
        if (!defaultProfilingStrategy.equals(strategy) && !strategies.contains(strategy)) {
            strategies.add(strategy);
        }
    }

    public static StackProfilingStrategy getDefaultStrategy() {
        return defaultProfilingStrategy;
    }

    public static boolean isActive() {
        if (defaultProfilingStrategy.isEnabled()) {
            return true;
        }
        for (ProfilingStrategy strategy : strategies) {
            if (strategy.isEnabled()) {
                return true;
            }
        }
        return false;
    }

    public static void pop(String name) {
        defaultProfilingStrategy.stop(name);
        for (ProfilingStrategy strategy : strategies) {
            strategy.stop(name);
        }
    }

    public static void push(String name) {
        if (defaultProfilingStrategy.isEnabled()) {
            defaultProfilingStrategy.start(name);
        }

        for (ProfilingStrategy strategy : strategies) {
            if (strategy.isEnabled()) {
                strategy.start(name);
            }
        }
    }

    public static void remove(ProfilingStrategy strategy) {
        strategies.remove(strategy);
    }
}
