package com.atlassian.util.profiling.strategy;

import com.atlassian.util.profiling.ProfilerConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timers;

import javax.annotation.Nonnull;

/**
 * Strategy for profiling code blocks, to be used with {@link Timers}.
 *
 * @see Timers
 * @since 3.0
 */
public interface ProfilerStrategy {

    /**
     * Method that is called to signal that the request has completed. Implementations of this method should clean up
     * any request or thread scoped resources associated with profiling state, including {@link Ticker#close() closing}
     * any {@link Ticker tickers} that have not yet been closed.
     */
    default void onRequestEnd() {
    }

    /**
     * Method that is called when the strategy is {@link StrategiesRegistry#addProfilerStrategy(ProfilerStrategy) registered}.
     *
     * @param configuration the configuration
     */
    void setConfiguration(@Nonnull ProfilerConfiguration configuration);

    /**
     * Called to mark the end of a profiled block of code.
     *
     * @param frameName name describing the profiled block of code
     * @return a {@link Ticker ticker} that can be used to {@link Ticker#close() mark} the end of the profiled code
     *         block. Must be non-null, but can be {@link Ticker#NO_OP} if the strategy opts out of profiling.
     */
    @Nonnull
    Ticker start(@Nonnull String frameName);
}
