package com.atlassian.util.profiling.strategy.impl;

import com.atlassian.util.profiling.Ticker;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

/**
 * Represents a single frame in a {@link ProfilingTrace profiling trace}.
 *
 * @since 3.0
 */
class ProfilingFrame implements Ticker {

    private final List<ProfilingFrame> children;
    private final long startMemory;
    private final long startTimeNanos;
    private final ProfilingTrace trace;

    private int count;
    private long durationNanos;
    private long memoryDelta;
    private String name;
    private ProfilingFrame parent;

    ProfilingFrame(@Nonnull ProfilingTrace trace, @Nonnull String name, boolean captureMemory) {
        this.name = requireNonNull(name, "name");
        this.trace = requireNonNull(trace, "trace");

        children = new ArrayList<>();
        count = 1;
        durationNanos = -1L;
        startMemory = captureMemory ? getUsedMemory() : -1L;
        startTimeNanos = System.nanoTime();
    }

    public void close() {
        durationNanos = System.nanoTime() - startTimeNanos;
        if (startMemory != -1L) {
            memoryDelta = getUsedMemory() - startMemory;
        }

        if (parent != null) {
            // merge the frame with its preceding sibling if they're identical
            List<ProfilingFrame> siblings = parent.children;
            if (siblings.size() > 1) {
                int index = siblings.lastIndexOf(this);
                if (index > 0 && siblings.get(index - 1).maybeMerge(this)) {
                    siblings.remove(index);
                }
            }
        }

        // prune this frame if it did not exceed the minimum frame time and remove it from its parent if there is one
        if (count > 0 && durationNanos < trace.getConfiguration().getMinFrameTime(TimeUnit.NANOSECONDS) &&
                (parent == null || parent.removeChild(this))) {
            count = 0;
        }

        // notify the trace that this frame is no longer active
        trace.onClose(this);
    }

    void addChild(@Nonnull ProfilingFrame child) {
        children.add(child);
        child.parent = this;
    }

    void append(@Nonnull String indent, @Nonnull StringBuilder builder) {
        double durationMillis = (durationNanos / 1_000_000d);
        builder.append(indent);
        builder.append("[").append(String.format("%.1f", durationMillis)).append("ms] ");
        if (count > 1) {
            builder.append("[count: ").append(count).append(", avg: ")
                    .append(String.format("%.1f", durationMillis / count)).append("ms] ");
        }
        if (startMemory != -1L) {
            builder.append("[").append(memoryDelta / 1024).append("KB used] ");
            builder.append("[").append(Runtime.getRuntime().freeMemory() / 1024).append("KB free] ");
        }

        builder.append("- ").append(name);
        builder.append("\n");

        String childIndent = indent + " ";
        for (ProfilingFrame child : children) {
            child.append(childIndent, builder);
        }
    }

    void closeAbnormally() {
        name = name + " (not closed)";
        close();
    }

    long getDurationNanos() {
        return durationNanos;
    }

    String getName() {
        return name;
    }

    ProfilingFrame getParent() {
        return parent;
    }

    boolean isPruned() {
        // when a frame is dropped or merged with another frame, its count is reduced
        return count == 0;
    }

    int size() {
        int size = 1;
        for (ProfilingFrame child : children) {
            size += child.size();
        }
        return size;
    }

    private static long getUsedMemory() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    /*
     * Merges this frame with the provided frame iff the frames are identical sub-trees (based on frame names). Merging
     * identical frames significantly compacts the profiling trace for patterns where a profiled block is called n
     * times in a loop, which results in more readable profiling logs and reduced memory usage.
     */
    private boolean maybeMerge(ProfilingFrame other) {
        if (other.count > 0 && treeEquals(other)) {
            treeMerge(other);
            return true;
        }

        return false;
    }

    private boolean removeChild(ProfilingFrame child) {
        // we're almost always going to remove the _last_ element in the children list when frames are dropped because
        // they were merged, or because the frame took less then the frame threshold time to complete.
        int index = children.lastIndexOf(child);
        if (index != -1) {
            children.remove(index);
            return true;
        }
        return false;
    }

    private boolean treeEquals(ProfilingFrame other) {
        int numberOfChildren = children.size();
        if (!(Objects.equals(name, other.name) && numberOfChildren == other.children.size())) {
            return false;
        }

        for (int i = 0; i < numberOfChildren; ++i) {
            if (!children.get(i).treeEquals(other.children.get(i))) {
                return false;
            }
        }

        return true;
    }

    private void treeMerge(ProfilingFrame other) {
        count++;
        other.count--;
        durationNanos += other.durationNanos;
        memoryDelta += other.memoryDelta;
        int numberOfChildren = children.size();
        for (int i = 0; i < numberOfChildren; ++i) {
            children.get(i).treeMerge(other.children.get(i));
        }
    }
}
