package com.atlassian.util.profiling.filters;

import com.atlassian.util.profiling.ProfilerConfiguration;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Filter that will intercept requests &amp; time how long it takes for them to return.  It stores this information
 * in the ProfilingTimerBean.
 * <p>
 * Install the filter in your web.xml file as follows:
 * <pre>
 *   &lt;filter&gt;
 *      &lt;filter-name&gt;profiling&lt;/filter-name&gt;
 *      &lt;filter-class&gt;com.atlassian.util.profiling.filters.ProfilingFilter&lt;/filter-class&gt;
 *      &lt;init-param&gt;
 *             &lt;param-name&gt;activate.param&lt;/param-name&gt;
 *             &lt;param-value&gt;profilingfilter&lt;/param-value&gt;
 *         &lt;/init-param&gt;
 *         &lt;init-param&gt;
 *             &lt;param-name&gt;autostart&lt;/param-name&gt;
 *             &lt;param-value&gt;false&lt;/param-value&gt;
 *         &lt;/init-param&gt;
 *  &lt;/filter&gt;
 *
 *  &lt;filter-mapping&gt;
 *      &lt;filter-name&gt;profiling&lt;/filter-name&gt;
 *      &lt;url-pattern&gt;/*&lt;/url-pattern&gt;
 *  &lt;/filter-mapping&gt;
 * </pre>
 *
 * With the above settings you can turn the filter on by accessing any URL with the parameter
 * <code>profilingfilter=on</code>.eg:
 *
 * <pre>    http://mywebsite.com/a.jsp?<b><i>profilingfilter=on</i></b></pre>
 * <p>
 * The above settings also sets the filter to not start automatically upon startup.  This may be useful for
 * production, but you will most likely want to set this true in development.
 *
 * @author <a href="mailto:mike@atlassian.com">Mike Cannon-Brookes</a>
 * @author <a href="mailto:scott@atlassian.com">Scott Farquhar</a>
 *
 * @deprecated in 3.0 for removal in 4.0. Use the replacement {@link RequestProfilingFilter} that allows enabling
 *             profiling for individual requests
 */
@Deprecated
public class ProfilingFilter implements javax.servlet.Filter {

    private static final Logger log = LoggerFactory.getLogger(ProfilingFilter.class);

    /**
     * This is the parameter you pass to the init parameter &amp; specify in the web.xml file: eg.
     * <pre>
     * &lt;filter&gt;
     *   &lt;filter-name&gt;profile&lt;/filter-name&gt;
     *   &lt;filter-class&gt;com.atlassian.util.profiling.filters.ProfilingFilter&lt;/filter-class&gt;
     *  &lt;init-param&gt;
     *    &lt;param-name&gt;autostart&lt;/param-name&gt;
     *    &lt;param-value&gt;true&lt;/param-value&gt;
     *  &lt;/init-param&gt;
     * &lt;/filter&gt;
     *  </pre>
     */
    protected static final String AUTOSTART_PARAM = "autostart";

    protected final StatusUpdateStrategy statusUpdateStrategy;

    public ProfilingFilter() {
        this.statusUpdateStrategy = new ProfilingStatusUpdateViaRequestStrategy();
    }

    protected ProfilingFilter(StatusUpdateStrategy statusUpdateStrategy) {
        if (statusUpdateStrategy == null) {
            throw new IllegalArgumentException("statusUpdateStrategy must not be null!");
        }
        this.statusUpdateStrategy = statusUpdateStrategy;
    }

    public void destroy() {
    }

    /**
     * If the filter is on record start time, pass to filter chain, and then record total time on the return. Otherwise
     * just pass to the filter chain. The filter can be activated via a request through the
     * ProfilingStatusUpdateStrategy.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        statusUpdateStrategy.setStateViaRequest(request);

        //if filter is not on - then don't do any processing.
        if (!isFilterOn()) {
            chain.doFilter(request, response);
            return;
        }

        final String resource = getResourceName(request);
        try (Ticker ignored = Timers.start(resource)) {
            chain.doFilter(request, response);
        }
    }

    public void init(FilterConfig filterConfig) {
        //some servlet containers set this to be null
        if (filterConfig != null) {
            // init profiling: autostart
            String autostartParam = filterConfig.getInitParameter(AUTOSTART_PARAM);
            if (autostartParam != null) {
                if ("true".equals(autostartParam)) {
                    log.debug("[Filter: {}] defaulting to on [{}=true]", filterConfig.getFilterName(), AUTOSTART_PARAM);
                    turnProfilingOn();
                } else if ("false".equals(autostartParam)) {
                    log.debug("[Filter: {}] defaulting to off [{}=false]", filterConfig.getFilterName(), AUTOSTART_PARAM);
                    turnProfilingOff();
                } else {
                    log.debug("[Filter: {}] autostart value: {} is unknown no action taken]",
                            filterConfig.getFilterName(), autostartParam);
                }
            }

            if (statusUpdateStrategy instanceof FilterConfigAware) {
                ((FilterConfigAware) statusUpdateStrategy).configure(filterConfig);
            }
        }
    }

    private boolean isFilterOn() {
        return Timers.getConfiguration().isEnabled();
    }

    private String getResourceName(ServletRequest request) {
        //if an include file then get the proper resource name.
        if (request.getAttribute("javax.servlet.include.request_uri") != null) {
            return (String) request.getAttribute("javax.servlet.include.request_uri");
        } else {
            return ((HttpServletRequest) request).getRequestURI();
        }
    }

    protected void turnProfilingOn() {
        Timers.getConfiguration().setEnabled(true);
    }

    protected void turnProfilingOff() {
        ProfilerConfiguration  config = Timers.getConfiguration();
        config.setMinTraceTime(0, TimeUnit.MILLISECONDS);
        config.setEnabled(false);
    }
}

