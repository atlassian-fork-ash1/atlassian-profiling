package com.atlassian.util.profiling;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A data class representing a single metric tag, as a key-value pair.
 *
 * @since 3.4
 */
@Immutable
@ParametersAreNonnullByDefault
public final class MetricTag {
    private final String key;
    private final String value;

    private MetricTag(String key, String value) {
        this.key = requireNonNull(key);
        this.value = requireNonNull(value);
    }

    @Nonnull
    public String getKey() {
        return key;
    }

    @Nonnull
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return key + "=" + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetricTag)) return false;
        MetricTag metricTag = (MetricTag) o;
        return key.equals(metricTag.key) &&
                value.equals(metricTag.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    /**
     * @since 3.4.2
     */
    @Nonnull
    public static MetricTag of(String key, String value) {
        return new MetricTag(key, value);
    }

    /**
     * @since 3.4.2
     */
    @Nonnull
    public static MetricTag of(String key, int value) {
        return new MetricTag(key, String.valueOf(value));
    }

    /**
     * @since 3.4.2
     */
    @Nonnull
    public static MetricTag of(String key, boolean value) {
        return new MetricTag(key, String.valueOf(value));
    }
}
