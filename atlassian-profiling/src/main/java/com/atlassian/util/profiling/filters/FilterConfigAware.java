package com.atlassian.util.profiling.filters;

import javax.servlet.FilterConfig;

/**
 * @deprecated in 3.0 for removal in 4.0
 */
@Deprecated
public interface FilterConfigAware {
    void configure(FilterConfig filterConfig);
}
