package com.atlassian.util.profiling;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Object that is used to collect time metrics about the execution a block of code.
 *
 * <pre>
 *     MetricTimer timer = Metrics.timer("my-metric");
 *     try (Ticker ignored = timer.start()) {
 *         // monitored code block
 *     }
 * </pre>
 *
 * or
 *
 * <pre>
 *     MetricTimer timer = Metrics.timer("my-metric");
 *     timer.update(5, TimeUnit.MILLISECONDS);
 * </pre>
 *
 * @since 3.0
 */
@ParametersAreNonnullByDefault
public interface MetricTimer {

    /**
     * Starts a {@link Ticker ticker} for a monitored code block
     *
     * @return the ticker
     */
    @Nonnull
    Ticker start();

    /**
     * Updates the metric with an additional value
     *
     * @param time     the time to record
     * @param timeUnit the time unit of {@code time}
     */
    default void update(long time, TimeUnit timeUnit) {
        update(Duration.ofNanos(timeUnit.toNanos(time)));
    }

    /**
     * Updates the metric with an additional value
     *
     * @param time     the time to record
     * @since 3.4
     */
    void update(Duration time);
}
