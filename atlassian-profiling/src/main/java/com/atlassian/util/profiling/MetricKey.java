package com.atlassian.util.profiling;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

/**
 * A data class representing the identifier of a single metric.
 *
 * @since 3.4
 */
@ParametersAreNonnullByDefault
@Immutable
public final class MetricKey {

    private final String metricName;
    private final Collection<MetricTag> tags;

    private MetricKey(String metricName, Collection<MetricTag> tags) {
        this.metricName = requireNonNull(metricName);
        this.tags = tags;
    }

    @Nonnull
    public String getMetricName() {
        return metricName;
    }

    @Nonnull
    public Collection<MetricTag> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return tags.isEmpty() ? metricName : metricName + getFormattedTags();
    }

    private String getFormattedTags() {
        return tags.stream().map(MetricTag::toString).collect(joining(",", "[", "]"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetricKey)) return false;
        MetricKey metricKey = (MetricKey) o;
        return metricName.equals(metricKey.metricName) &&
                tags.equals(metricKey.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metricName, tags);
    }

    public static MetricKey metricKey(String metricName) {
        return new MetricKey(metricName, emptyList());
    }

    public static MetricKey metricKey(String metricName, Collection<MetricTag> tags) {
        return new MetricKey(metricName, new ArrayList<>(tags));
    }

    public static MetricKey metricKey(String metricName, MetricTag... tags) {
        return new MetricKey(metricName, asList(tags));
    }
}
