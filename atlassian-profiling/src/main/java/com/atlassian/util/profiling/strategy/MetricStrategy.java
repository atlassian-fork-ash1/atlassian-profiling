package com.atlassian.util.profiling.strategy;

import com.atlassian.util.profiling.Histogram;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTimer;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timers;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * Strategy for tracking metrics of profiled code blocks, to be used with {@link Metrics}.
 * <p>
 * Only {@link MetricTimer metric timers} and {@link Histogram histograms} created through {@link Metrics#timer(String)},
 * {@link Metrics#startTimer(String)}, {@link Metrics#histogram(String)}, {@link Timers#timerWithMetric} and
 * {@link Timers#startWithMetric} will interact with
 * {@link StrategiesRegistry#addMetricStrategy(MetricStrategy) configured} {@code MetricStrategy} instances.
 *
 * @see Metrics
 * @see Timers
 * @since 3.0
 */
@ParametersAreNonnullByDefault
public interface MetricStrategy {

    /**
     * Method that is called to signal that the request has completed. Implementations of this method should clean up
     * any request or thread scoped resources associated with profiling state, including {@link Ticker#close() closing}
     * any {@link Ticker tickers} that have not yet been closed.
     */
    default void onRequestEnd() {
    }

    /**
     * Method that is called when the strategy is {@link StrategiesRegistry#addMetricStrategy(MetricStrategy) registered}.
     *
     * @param configuration the configuration
     */
    default void setConfiguration(MetricsConfiguration configuration) {
    }

    /**
     * Called to mark the end of a block of code for which metrics should be calculated.
     *
     * @param metricName the name of metric
     * @return a {@link Ticker ticker} that can be used to {@link Ticker#close() mark} the end of the code block for
     * which metrics should be calculated. Must be non-null, but can be {@link Ticker#NO_OP} if the strategy
     * opts out of tracking.
     */
    @Nonnull
    Ticker startTimer(String metricName);

    /**
     * Called to mark the end of a block of code for which metrics should be calculated.
     *
     * @param metricKey the key of metric
     * @return a {@link Ticker ticker} that can be used to {@link Ticker#close() mark} the end of the code block for
     * which metrics should be calculated. Must be non-null, but can be {@link Ticker#NO_OP} if the strategy
     * opts out of tracking.
     * @since 3.4
     */
    @Nonnull
    default Ticker startTimer(MetricKey metricKey) {
        // for backwards compatibility, by default we just discard any tags in the key and just use the name.
        return startTimer(metricKey.getMetricName());
    }

    /**
     * Called when a histogram is {@link Histogram#update(long) updated}.
     *
     * @param metricName the histogram name
     * @param value      the value to record
     */
    void updateHistogram(String metricName, long value);

    /**
     * Called when a histogram is {@link Histogram#update(long) updated}.
     *
     * @param metricKey the histogram key
     * @param value     the value to record
     * @since 3.4
     */
    default void updateHistogram(MetricKey metricKey, long value) {
        // for backwards compatibility, by default we just discard any tags in the key and just use the name.
        updateHistogram(metricKey.getMetricName(), value);
    }

    /**
     * Called when a metric is {@link MetricTimer#update(long, TimeUnit) updated}.
     *
     * @param metricName the name of metric
     * @param time       the time to record
     * @param timeUnit   the time unit of {@code time}
     */
    void updateTimer(String metricName, long time, TimeUnit timeUnit);

    /**
     * Called when a metric is {@link MetricTimer#update(Duration) updated}.
     *
     * @param metricKey the key of timer
     * @param time      the time to record
     * @since 3.4
     */
    default void updateTimer(MetricKey metricKey, Duration time) {
        // for backwards compatibility, by default we just discard any tags in the key and just use the name.
        updateTimer(metricKey.getMetricName(), time.toNanos(), NANOSECONDS);
    }
}
