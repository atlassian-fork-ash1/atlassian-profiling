package com.atlassian.util.profiling.filters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.util.profiling.Timers.getConfiguration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @deprecated in 3.0 for removal in 4.0
 */
@Deprecated
@RunWith(MockitoJUnitRunner.class)
public class ProfilingStatusViaRequestUpdateStrategyTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private FilterConfig filterConfig;
    private ProfilingStatusUpdateViaRequestStrategy defaultStrategy;
    private boolean wasEnabled;

    @Before
    public void setup() {
        setDefaultStrategy();
        wasEnabled = isEnabled();
    }

    @After
    public void tearDown() {
        getConfiguration().setEnabled(wasEnabled);
    }

    @Test
    public void testTurnOnProfiling() {
        when(request.getQueryString()).thenReturn("profile.filter=on&spaceKey=TST");
        defaultStrategy.setStateViaRequest(request);
        assertTrue(isEnabled());
    }

    public void testTurnOnProfilingWithThreshhold() {
        when(request.getQueryString()).thenReturn("pageId=10000&profile.filter=20&spaceKey=TST");
        defaultStrategy.setStateViaRequest(request);
        assertTrue(isEnabled());
    }

    public void testTurnOffProfiling() {
        when(request.getQueryString()).thenReturn("pageId=10000&profile.filter=off");
        defaultStrategy.setStateViaRequest(request);
        assertFalse(isEnabled());
    }

    public void testIgnoreAcivation() {
        getConfiguration().setEnabled(true);
        defaultStrategy.setStateViaRequest(request);
        assertTrue(isEnabled());
    }

    public void testChangeInitParam() {
        when(filterConfig.getInitParameter(ProfilingStatusUpdateViaRequestStrategy.ON_OFF_INIT_PARAM)).thenReturn("profile");
        defaultStrategy.configure(filterConfig);
        assertEquals("profile=([\\w\\d]+)", ProfilingStatusUpdateViaRequestStrategy.getOnOffParameterPattern().pattern());
    }

    private boolean isEnabled() {
        return getConfiguration().isEnabled();
    }

    private void setDefaultStrategy() {
        defaultStrategy = new ProfilingStatusUpdateViaRequestStrategy() {
            public FilterConfig getFilterConfig() {
                return filterConfig;
            }
        };
    }
}
